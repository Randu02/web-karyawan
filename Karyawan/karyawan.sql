-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2020 at 02:16 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyawan`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_karyawan`
--

CREATE TABLE `data_karyawan` (
  `NIK` int(25) NOT NULL,
  `Nama` varchar(50) NOT NULL,
  `Tempat_tgl` varchar(50) NOT NULL,
  `JK` varchar(25) NOT NULL,
  `Alamat` varchar(100) NOT NULL,
  `Agama` varchar(10) NOT NULL,
  `Status_perkawinan` varchar(25) NOT NULL,
  `Pekerjaan` varchar(35) NOT NULL,
  `Gaji` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_karyawan`
--

INSERT INTO `data_karyawan` (`NIK`, `Nama`, `Tempat_tgl`, `JK`, `Alamat`, `Agama`, `Status_perkawinan`, `Pekerjaan`, `Gaji`) VALUES
(21233, 'randuu', 'jakarta , 2020-02-10', 'Laki-laki', 'kebon jeruk', 'islam', 'BELUM KAWIN', 'kantorann', 3400000),
(32123, 'randu', 'Jakarta, 2002-02-10', 'Laki-laki', 'gang cempaka no 4', 'islam', 'BELUM KAWIN', 'kantoran', 30000),
(32222, 'randuu', 'jakarta , 2020-02-10', 'Laki-laki', 'kebon jeruk', 'islam', 'BELUM KAWIN', 'akuntan', 40000000),
(3242342, 'werwerw', 'safadfs', 'Perempuan', 'asdasfa', 'islam', 'BELUM KAWIN', 'akuntan', 123231);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_karyawan`
--
ALTER TABLE `data_karyawan`
  ADD PRIMARY KEY (`NIK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
