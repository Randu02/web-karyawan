package org.example;

import java.sql.*;
import java.util.ArrayList;

public class ConnectionDb {
    static String db = "jdbc:mysql://localhost:3306/karyawan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    Connection connection;
    ArrayList datas = new ArrayList();

    public Connection runConnection(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(db,"root", "");

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

}
