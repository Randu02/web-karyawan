package org.example;


import org.json.JSONObject;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Karyawan {
    Connection connection = new ConnectionDb().runConnection();
    ArrayList datas = new ArrayList();


    void query(String theJSON) throws SQLException {
        JSONObject theResponse = new JSONObject(theJSON);

        if (theResponse.getString("type").equalsIgnoreCase("data")) {
            String Nik = theResponse.getString("nik");
            String nama = theResponse.getString("nama");
            String ttl = theResponse.getString("ttl");
            String jk = theResponse.getString("jk");
            String alamat = theResponse.getString("alamat");
            String agama = theResponse.getString("agama");
            String stts = theResponse.getString("stts");
            String pekerjaan = theResponse.getString("pekerjaan");
            String gaji = theResponse.getString("gaji");


            queryIn(Nik, nama, ttl, jk, alamat, agama, stts, pekerjaan, gaji);
        }
    }

    void queryIn(String Nik, String nama, String ttl, String jk,String alamat,String agama, String stts, String pekerjaan,String gaji) throws SQLException {

        String query = "INSERT INTO data_karyawan VALUES ('"+Nik+"', '"+nama+"', '"+ttl+"', '"+jk+"', '"+alamat+"','"+agama+"','"+stts+"','"+pekerjaan+"','"+gaji+"')";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate(query);
    }
    void showData() throws SQLException {
        String query = "SELECT * FROM data_karyawan";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery(query);
        while (resultSet.next()){
            int Nik = resultSet.getInt("NIK");
            String nama = resultSet.getString("Nama");
            String tempat = resultSet.getString("Tempat_tgl");
            String jk = resultSet.getString("JK");
            String alamat = resultSet.getString("alamat");
            String agama =resultSet.getString("agama");
            String stts = resultSet.getString("status_perkawinan");
            String pekerjaan =resultSet.getString("pekerjaan");
            String gaji =resultSet.getString("gaji");

            String response = "{\"Nik\" : \""+ Nik +"\", \"nama\" : \""+ nama +"\", \"tempat\" : \""+ tempat +"\", \"JK\" : \""+ jk +"\", \"alamat\" : \""+ alamat +"\",\"agama\" : \""+agama+"\",\"stts\" : \""+stts+"\",\"pekerjaan\" : \""+pekerjaan+"\",\"gaji\" : \""+gaji+"\"}";
//            System.out.println(response);
            datas.add(response);
            System.out.println(response);
        }
    }

    ArrayList<String> getDatas() throws SQLException {
        showData();
        return datas;
    }

}
